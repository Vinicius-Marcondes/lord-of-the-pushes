const theme = {
    colors: {
        background: '#e1e1e6',
        primary: '#BC0F0F',
        secondary: '#242527',
    },
};

export default theme;
