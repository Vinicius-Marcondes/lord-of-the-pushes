# Lord Of The Pushes 👋

> Development in Next/React

## Libraries

- [Example](https://google.com)

## Methodologies

- [Components](https://atomicdesign.bradfrost.com/chapter-2)

## Development environment

### Install dependencies

```bash
yarn install
```

### Run application

```bash
yarn dev
```

Access the application via url: [Link](http://localhost:3000).
