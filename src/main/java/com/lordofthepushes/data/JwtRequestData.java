package com.lordofthepushes.data;

import java.io.Serializable;

public class JwtRequestData implements Serializable {

    final private String username;
    final private String password;

    public JwtRequestData(final String username, final String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
