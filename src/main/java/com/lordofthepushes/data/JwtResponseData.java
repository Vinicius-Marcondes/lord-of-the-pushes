package com.lordofthepushes.data;

import java.io.Serializable;

public class JwtResponseData implements Serializable {
    private final String jwtToken;

    public JwtResponseData(final String jwtToken) {
        this.jwtToken = jwtToken;
    }

    public String getJwtToken() {
        return jwtToken;
    }
}
