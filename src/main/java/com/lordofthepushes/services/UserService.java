package com.lordofthepushes.services;

import com.lordofthepushes.data.UserData;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface UserService extends UserDetailsService {
    void saveUser(UserData userData);
    void updateUser(UserData userData);
    void deleteUser(Long userId);
    UserData getUserById(Long userId);
    Iterable<UserData> getAllUsers();
    Iterable<UserData> getAllUsers(Pageable page);
    UserData getUserByEmail(final String email);

    @Override
    UserDetails loadUserByUsername(String s) throws UsernameNotFoundException;
}
