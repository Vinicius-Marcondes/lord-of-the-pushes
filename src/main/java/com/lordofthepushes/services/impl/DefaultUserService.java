package com.lordofthepushes.services.impl;

import com.lordofthepushes.dao.CharacterDAO;
import com.lordofthepushes.exceptions.UnknownIdentifierException;
import com.lordofthepushes.dao.UserDAO;
import com.lordofthepushes.data.UserData;
import com.lordofthepushes.services.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Collections;
import java.util.Optional;

@Service
public class DefaultUserService implements UserService {
    private static final Logger LOG = LogManager.getLogger(DefaultUserService.class);

    private final UserDAO userDAO;

    public DefaultUserService(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    public void saveUser(UserData userData) {
        userDAO.save(userData);
    }

    @Override
    public void updateUser(UserData userData) {
        saveUser(userData);
    }

    @Override
    public void deleteUser(Long userId) {
        Assert.notNull(userId, "User Id must no be null!");
        UserData user = getUserById(userId);
        user.setActive(false);
        saveUser(user);
    }

    @Override
    public UserData getUserById(Long userId) {
        Assert.notNull(userId, "User Id must not be null!");
        Optional<UserData> user = userDAO.findById(userId);
        if (user.isEmpty()) {
            throw new UnknownIdentifierException("User " + userId + " not found!");
        }

        return user.get();
    }

    @Override
    public UserData getUserByEmail(final String email) {
        return userDAO.findByEmail(email);
    }

    @Override
    public Iterable<UserData> getAllUsers() {
        return userDAO.findAll();
    }

    @Override
    public Iterable<UserData> getAllUsers(Pageable page) {
        return userDAO.findAll(page);
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        final UserData user = getUserByEmail(email);

        if (user.getEmail().equals(email)) {
            return new User(user.getEmail(), user.getPassword(), Collections.emptyList());
        } else {
            throw new UsernameNotFoundException("User not found with email: " + email);
        }
    }
}
