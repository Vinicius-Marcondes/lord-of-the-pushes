package com.lordofthepushes.controllers;

import com.lordofthepushes.data.JwtRequestData;
import com.lordofthepushes.data.JwtResponseData;
import com.lordofthepushes.security.JwtTokenUtil;
import com.lordofthepushes.services.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@CrossOrigin
@RestController
public class JwtAuthenticationController {

    private final JwtTokenUtil jwtTokenUtil;
    private final AuthenticationManager authenticationManager;
    private final UserService jwtUserDetailsService;

    public JwtAuthenticationController(final JwtTokenUtil jwtTokenUtil, final AuthenticationManager authenticationManager, final UserService jwtUserDetailsService) {
        this.jwtTokenUtil = jwtTokenUtil;
        this.authenticationManager = authenticationManager;
        this.jwtUserDetailsService = jwtUserDetailsService;
    }

    @RequestMapping(value = "/auth", method = RequestMethod.POST)
    public ResponseEntity<JwtResponseData> createAuthenticationToken(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse, @RequestBody final JwtRequestData authenticationRequestData) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authenticationRequestData.getUsername(), authenticationRequestData.getPassword()));

            final UserDetails userDetails = jwtUserDetailsService.loadUserByUsername(authenticationRequestData.getUsername());
            final String token = jwtTokenUtil.generateToken(userDetails);
            final Cookie cookie = new Cookie("Authorization", token);
            return ResponseEntity.ok(new JwtResponseData(token));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}
